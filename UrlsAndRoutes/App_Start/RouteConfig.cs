﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UrlsAndRoutes
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);

            //Route myRoute = new Route("{controller}/{action}",new MvcRouteHandler());
            //routes.Add(myRoute);

            //routes.MapRoute(null, "X{controller}/{action}");

            //routes.MapRoute(null, "Shop/SomeOldMethod",
            //    defaults: new {action = "Index", controller = "Home"});

            //routes.MapRoute(null, "Shop/{action}",
            //    defaults: new {action = "Index", controller = "Home"});

            //routes.MapRoute(null, "Public/{controller}/{action}",
            //    defaults: new {action = "Index", controller = "Home"});

            //routes.MapRoute("MyRoute", "{controller}/{action}",
            //    defaults: new {action = "Index", controller = "Home"});

            routes.MapRoute(
                name: "MyRoute",
                url: "{controller}/{action}/{id}/{*catchall}",
                defaults: new {controller = "Home", action = "Index", id = UrlParameter.Optional},
                namespaces: new []{"UrlsAndRoutes.AdditionalControllers"});
        }
    }
}
